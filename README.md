# Metasploitable Lab in docker-compose
This is a quick re-write of the msf/metasploitable lab using docker-compose.

The docker-compose includes:
- msf: the official metasploit framework image
- db: a postgre database for persistent storage in MSF
- target: a public non-official copy of metasploitable in docker, most of the services work but a few do not


docker-compose notes:
- msf container runs a useless, non-terminating command so that we can favor using exec over run for ease of use


## Usage
Setup db and target:
```
$ docker-compose up -d
```

Run a console instance:
```
$ docker-compose exec msf ./msfconsole
```

Run a shell on an already running msf container, useful for doing stuff outside of msfconsole:
```
$ docker-compose exec msf bash
```

Tear everything down when done:
```
$ docker-compose down
```

